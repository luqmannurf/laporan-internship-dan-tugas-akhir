\contentsline {chapter}{COVER}{i}
\contentsline {chapter}{LEMBAR PENGESAHAN LAPORAN}{iii}
\contentsline {chapter}{LEMBAR PERNYATAAN}{iv}
\contentsline {chapter}{HALAMAN PERNYATAAN}{v}
\contentsline {chapter}{ABSTRAK}{vi}
\contentsline {chapter}{\textit {ABSTRACT}}{vii}
\contentsline {chapter}{KATA PENGANTAR}{viii}
\contentsline {chapter}{DAFTAR ISI}{ix}
\contentsline {chapter}{DAFTAR GAMBAR}{xiii}
\contentsline {chapter}{DAFTAR TABEL}{xiv}
\contentsline {chapter}{DAFTAR SIMBOL}{xv}
\contentsline {chapter}{\numberline {I}Pendahuluan}{1}
\contentsline {section}{\numberline {1.1}Identifikasi Masalah}{3}
\contentsline {section}{\numberline {1.2}Tujuan dan Manfaat}{3}
\contentsline {section}{\numberline {1.3}Ruang Lingkup}{3}
\contentsline {chapter}{DAFTAR SINGKATAN}{1}
\contentsline {chapter}{\numberline {II}Landasan Teori}{4}
\contentsline {section}{\numberline {2.1}Deskripsi Teori Yang Sama}{4}
\contentsline {subsection}{\numberline {2.1.1}Prediksi}{4}
\contentsline {subsection}{\numberline {2.1.2}Asumsi Klasik}{4}
\contentsline {subsection}{\numberline {2.1.3}Tanaman}{4}
\contentsline {subsection}{\numberline {2.1.4}Produksi}{5}
\contentsline {subsection}{\numberline {2.1.5}Padi}{5}
\contentsline {subsection}{\numberline {2.1.6}Data Mining}{5}
\contentsline {subsection}{\numberline {2.1.7}PHP}{6}
\contentsline {section}{\numberline {2.2}Deskripsi Metode Yang Sama}{6}
\contentsline {subsection}{\numberline {2.2.1}Metode Penelitian}{6}
\contentsline {subsubsection}{\numberline {2.2.1.1}Regresi Linear}{6}
\contentsline {subsection}{\numberline {2.2.2}Regresi Linear Sederhana}{6}
\contentsline {subsection}{\numberline {2.2.3}\textit {Exponential Smoothing}}{7}
\contentsline {subsection}{\numberline {2.2.4}Studi Kepustakaan}{8}
\contentsline {subsection}{\numberline {2.2.5}Metode Pengumpulan Data}{8}
\contentsline {subsubsection}{\numberline {2.2.5.1}Data}{8}
\contentsline {subsubsection}{\numberline {2.2.5.2}Jenis Data}{8}
\contentsline {subsubsection}{\numberline {2.2.5.3}Sumber Data}{8}
\contentsline {subsubsection}{\numberline {2.2.5.4}Metode Pengolahan Data}{8}
\contentsline {subsubsection}{\numberline {2.2.5.5}Membuat Kesimpulan}{8}
\contentsline {section}{\numberline {2.3}Studi Pustaka}{9}
\contentsline {chapter}{\numberline {III}Gambaran Objek Studi}{11}
\contentsline {section}{\numberline {3.1}Objek Studi}{11}
\contentsline {subsection}{\numberline {3.1.1}Informasi Perusahaan}{11}
\contentsline {subsection}{\numberline {3.1.2}Visi Dan Misi Perusahaan}{11}
\contentsline {subsubsection}{\numberline {3.1.2.1}Visi Perusahaan}{11}
\contentsline {subsubsection}{\numberline {3.1.2.2}Misi Perusahaan}{11}
\contentsline {section}{\numberline {3.2}Referensi Data}{12}
\contentsline {subsection}{\numberline {3.2.1}Referensi Data Primer}{12}
\contentsline {subsection}{\numberline {3.2.2}Referensi Data Sekunder}{12}
\contentsline {chapter}{\numberline {IV}Metodologi Penelitian}{13}
\contentsline {section}{\numberline {4.1}Flowchart Metodologi Penelitian}{13}
\contentsline {section}{\numberline {4.2}Deskripsi Metodologi}{13}
\contentsline {subsection}{\numberline {4.2.1}Identifikasi Masalah dan Tujuan}{13}
\contentsline {subsection}{\numberline {4.2.2}Pengumpulan Data}{14}
\contentsline {section}{\numberline {4.3}Analisis}{14}
\contentsline {subsection}{\numberline {4.3.1}Regresi Linear}{14}
\contentsline {subsubsection}{\numberline {4.3.1.1}Mengidentifikasi Variabel}{14}
\contentsline {subsubsection}{\numberline {4.3.1.2}Membuat Tabel}{15}
\contentsline {subsubsection}{\numberline {4.3.1.3}Menghitung koefisien (a) dan (b)}{15}
\contentsline {subsubsection}{\numberline {4.3.1.4}Melakukan Peramalan}{15}
\contentsline {subsubsection}{\numberline {4.3.1.5}Menghitung Hasil MSE}{15}
\contentsline {subsection}{\numberline {4.3.2}\textit {Exponential Smoothing}}{16}
\contentsline {subsubsection}{\numberline {4.3.2.1}Studi Kepustakaan}{16}
\contentsline {subsubsection}{\numberline {4.3.2.2}Menentukan Topik Permasalahan}{16}
\contentsline {subsubsection}{\numberline {4.3.2.3}Pengumpulan Data}{16}
\contentsline {subsubsection}{\numberline {4.3.2.4}Menghitung \textit {Eksponential} Tunggal}{16}
\contentsline {subsubsection}{\numberline {4.3.2.5}Menghitung \textit {Exponential} Ganda}{16}
\contentsline {subsubsection}{\numberline {4.3.2.6}Menghitung Koefisien A Dan B}{17}
\contentsline {subsubsection}{\numberline {4.3.2.7}Menghitung Tren Peramalan F}{17}
\contentsline {subsubsection}{\numberline {4.3.2.8}Menghitung Nilai Kesalahan E}{17}
\contentsline {subsubsection}{\numberline {4.3.2.9}Menghitung MSE}{18}
\contentsline {subsubsection}{\numberline {4.3.2.10}Penentuan Bentuk Peralaman Ft+m}{18}
\contentsline {subsection}{\numberline {4.3.3}Desain}{18}
\contentsline {subsection}{\numberline {4.3.4}Implementasi}{18}
\contentsline {subsection}{\numberline {4.3.5}Kesimpulan}{18}
\contentsline {chapter}{\numberline {V}Pengujian Dan Hasil}{19}
\contentsline {section}{\numberline {5.1}Pengujian}{19}
\contentsline {subsection}{\numberline {5.1.1}Regresi Linear}{20}
\contentsline {subsubsection}{\numberline {5.1.1.1}Mengidentifikasi Variabel}{20}
\contentsline {subsection}{\numberline {5.1.2}Membuat Tabel}{20}
\contentsline {subsection}{\numberline {5.1.3}Menghitung koefisien (a) dan (b)}{20}
\contentsline {subsection}{\numberline {5.1.4}Melakukan Peramalan}{21}
\contentsline {subsection}{\numberline {5.1.5}Menghitung Hasil MSE}{22}
\contentsline {section}{\numberline {5.2}\textit {Exponential Smoothing}}{22}
\contentsline {subsection}{\numberline {5.2.1}Menghitung Exponential Tunggal}{22}
\contentsline {subsection}{\numberline {5.2.2}Menghitung \textit {Exponential} Ganda}{23}
\contentsline {subsection}{\numberline {5.2.3}Menghitung Koefisien A Dan B}{24}
\contentsline {subsection}{\numberline {5.2.4}Menghitung Tren Peramalan F}{25}
\contentsline {subsection}{\numberline {5.2.5}Menghitung Nilai Kesalahan E}{26}
\contentsline {subsection}{\numberline {5.2.6}Menghitung MSE}{27}
\contentsline {subsection}{\numberline {5.2.7}Penentuan bentuk peralaman Ft+m}{27}
\contentsline {section}{\numberline {5.3}Hasil}{28}
\contentsline {subsection}{\numberline {5.3.1}Perancangan Sistem}{28}
\contentsline {subsection}{\numberline {5.3.2}\textit {Use Case Diagram}}{28}
\contentsline {subsection}{\numberline {5.3.3}Definisi \textit {Use Case}}{29}
\contentsline {subsubsection}{\numberline {5.3.3.1}Definisi Aktor dan \textit {Use Case}}{29}
\contentsline {subsection}{\numberline {5.3.4}Skenario \textit {Use Case}}{29}
\contentsline {subsubsection}{\numberline {5.3.4.1}Skenario \textit {Use Case Login}}{29}
\contentsline {subsubsection}{\numberline {5.3.4.2}Skenario Kelola Data Produksi}{29}
\contentsline {subsubsection}{\numberline {5.3.4.3}Skenario Perhitungan}{29}
\contentsline {subsubsection}{\numberline {5.3.4.4}Skenario Pilih Tahun Prediksi}{29}
\contentsline {subsubsection}{\numberline {5.3.4.5}Skenario Grafik}{29}
\contentsline {subsection}{\numberline {5.3.5}\textit {Class Diagram}}{30}
\contentsline {subsection}{\numberline {5.3.6}\textit {Sequence Diagram}}{30}
\contentsline {subsubsection}{\numberline {5.3.6.1}\textit {Sequence Diagram Login}}{30}
\contentsline {subsubsection}{\numberline {5.3.6.2}\textit {Sequence Diagram} Data Produksi Padi}{30}
\contentsline {subsubsection}{\numberline {5.3.6.3}\textit {Sequence Diagram} Perhitungan}{30}
\contentsline {subsubsection}{\numberline {5.3.6.4}\textit {Sequence Diagram} Prediksi}{30}
\contentsline {subsubsection}{\numberline {5.3.6.5}\textit {Sequence Diagram} Grafik}{31}
\contentsline {subsection}{\numberline {5.3.7}\textit {Activity Diagram}}{31}
\contentsline {subsubsection}{\numberline {5.3.7.1}\textit {Activity Diagram }Data Produksi Padi}{31}
\contentsline {subsubsection}{\numberline {5.3.7.2}\textit {Activity Diagram }Perhitungan}{31}
\contentsline {subsubsection}{\numberline {5.3.7.3}\textit {Activity Diagram }Prediksi}{31}
\contentsline {subsubsection}{\numberline {5.3.7.4}\textit {Activity Diagram} Grafik}{32}
\contentsline {subsection}{\numberline {5.3.8}\textit {Component Diagram}}{32}
\contentsline {subsection}{\numberline {5.3.9}\textit {Deployment Diagram}}{32}
\contentsline {section}{\numberline {5.4}\textit {User Interface}}{32}
\contentsline {subsection}{\numberline {5.4.1}\textit {User Interface Login}}{32}
\contentsline {subsection}{\numberline {5.4.2}\textit {User Interface} Halaman Utama}{32}
\contentsline {subsection}{\numberline {5.4.3}\textit {User Interface} Kelola Data Produksi}{33}
\contentsline {section}{\numberline {5.5}Implementasi}{33}
\contentsline {subsection}{\numberline {5.5.1}Lingkungan Implementasi}{33}
\contentsline {subsection}{\numberline {5.5.2}Tampilan Antar Muka}{34}
\contentsline {subsubsection}{\numberline {5.5.2.1}Halaman Login}{34}
\contentsline {subsubsection}{\numberline {5.5.2.2}Halaman Utama}{34}
\contentsline {subsubsection}{\numberline {5.5.2.3}Halaman Kelola Data Produksi Padi}{34}
\contentsline {subsubsection}{\numberline {5.5.2.4}Halaman Perhitungan}{34}
\contentsline {subsubsection}{\numberline {5.5.2.5}Halaman Grafik}{34}
\contentsline {subsubsection}{\numberline {5.5.2.6}Halaman Prediksi}{34}
\contentsline {subsubsection}{\numberline {5.5.2.7}Halaman Hasil Prediksi}{35}
\contentsline {subsection}{\numberline {5.5.3}Pengujian \textit {Blackbox}}{35}
\contentsline {subsubsection}{\numberline {5.5.3.1}Identifikasi dan Rencana Pengujian}{35}
\contentsline {chapter}{\numberline {VI}Kesimpulan}{48}
\contentsline {section}{\numberline {6.1}Kesimpulan Masalah}{48}
\contentsline {section}{\numberline {6.2}Kesimpulan Metodologi}{48}
\contentsline {section}{\numberline {6.3}Kesimpulan Pengujian}{48}
\contentsline {chapter}{\numberline {A}\textit {CURRICULUM VITAE}}{50}
\contentsline {chapter}{Daftar Pustaka}{50}
\contentsline {chapter}{\numberline {B}JURNAL}{51}
